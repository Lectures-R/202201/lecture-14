---
pagetitle: "Introducción a Web-scraping"
title: "\nTaller de R: Estadística y programación\n"
subtitle: "\nLectura 14: Introducción a Web-scraping\n"
author: "\nEduard F. Martínez-González\n"
date: "Universidad de los Andes | [ECON-1302](https://github.com/taller-R-202201)"
output: 
  revealjs::revealjs_presentation:  
    theme: simple 
    highlight: tango
    center: true
    nature:
      transition: slide
      self_contained: false # para que funcione sin internet
      highlightStyle: github
      highlightLines: true
      countIncrementalSlides: true
      showSlideNumber: 'all'
    seal: true # remover la primera diapositiva (https://github.com/yihui/xaringan/issues/84)
    # Help 1: https://revealjs.com/presentation-size/  
    # Help 2: https://bookdown.org/yihui/rmarkdown/revealjs.html
    # Estilos revealjs: https://github.com/hakimel/reveal.js/blob/master/css/theme/template/theme.scss
---
```{r setup, include=F , cache=F}
# load packages
require(pacman)
p_load(here,knitr,tidyverse,ggthemes,fontawesome)

# option html
options(htmltools.dir.version = F)
opts_chunk$set(fig.align="center", fig.height=4 , dpi=300 , cache=F)
```

## Hoy veremos

### **1.**  Introducción a Web-scraping

### **2.**  Aplicación en R

<!------------------------------>
<!------------------------------>
<!------------------------------>
## Prerequisitos

Para replicar esta clase es ideal que tenga instalada la versión de `4.1.1` de R
   
```{r,eval=T,include=T}
R.version.string # Versión de R
```

Además, debe instalar las siguientes librerías:

```{r,eval=T,include=T}
# llamar/instalar librerías de la clase
require(pacman)
p_load(tidyverse,data.table,plyr, # cargar y/o instalar paquetes a usar
       rvest, # web-scraìng
       XML,   # web-scraìng
       xml2)  # web-scraìng
```
  
<!---------------------------------------->
<!--- Introducción a datos geográficos --->
<!---------------------------------------->
# [1.] Introducción a Web-scraping
<html><div style='float:left'></div><hr color='#000099' size=3px width=850px></html>


<!---------------->
## ¿Qué es web-scraping?

Es una técnica usada para automatizar los procesos de extracción de información de sitios web, como tablas, textos o link a otras páginas.

#### ¿Por qué hacer web-scraping?

* Funciona mejor que copiar y pegar la información de la web
* Es rápido y replicable
* Se puede generalizar el proceso en la mayoría de los casos 

#### Antes de hacer web-scraping...

* Leer los términos y condiciones de la pagina web


<!---------------->
## ¿Qué es un HTML (Hyper Text Markup Language)?


* HTML o HyperText Markup Language no es un lenguaje de programación, HTML es un lenguaje de marcado de hipertexto.

* Se escribe en su totalidad con elementos, estos elementos están constituidos por etiquetas, contenido y atributos (mas adelante veremos que son). 

* Este lenguaje que es interpretado por el navegador web para mostrar las paginas web tal y como estamos acostumbrados a verlas.

* Esta estructurado como un árbol para el cual se puede rastrear cualquier la ruta de cualquier nodo o etiqueta.


<!---------------->
## ¿Cómo se ve un HTML?

<div align="center">
<img src="pics/view_html.gif" height=400>
</div>


<!---------------->
## Elementos de un HTML

Como dijimos anteriormente, HTML es un lenguaje formado por elementos, cada elemento esta conformado por la etiqueta, los atributos y el contenido. Así se ve un elemento:

`<p id="texto"> Hola mundo </p>`

Etiqueta: `<p>`
Atributos: `id="texto"`
Contenido: `Hola mundo`

#### Etiquetas 

Las etiquetas sirven para delimitar el inicio y el final de un elemento.

* Inicio de la etiqueta:   `< >`
* Fin de la etiqueta:   `<\ >`


<!---------------->
## Algunos ejemplos de etiquetas

* `<p>`: Párrafos
* `<head>`: Encabezado de la pagina
* `<body>`: Cuerpo de la pagina
* `<h1>, <h2>,...,<hi>`: Encabezados, Secciones
* `<a>`: links
* `<li>`: Ítem en una lista
* `<table>`: Tablas
* `<td>`: Una celda de datos en una tabla
* `<div>`: División. Sirve para crear secciones o agrupar contenidos.
* `<script>`: Se utiliza para insertar o hacer referencia a un script

#### Atributos del elemento

Los atributos proveen información adicional de un elemento y siempre se expresan en la etiqueta de inicio y se les asigna un nombre y un valor:

`<p id="texto">` Hola mundo `</p>`

Aquí el nombre del atributo es "id" y el valor es "texto". Nota: Un elemento puede tener varios atributos


<!---------------->
## Estructura de un HTML en una pagina web
```{r, eval=FALSE}
(1) ---->  <!DOCTYPE html> 
(2) ---->  <html>
(3) ---->  <head>
(4) ---->  <meta charset="utf-8">
(5) ---->  <title> Aquí va el título de la pagina</title>
           </head>
(6) ---->  <body>
           <h1>Aquí va nuestro titulo y esta en la etiqueta <u>h1</u>.</h1>
           <h2>Este es un subtítulo y se encuentra en la etiqueta <u>h2</u>.</h2>
           <p> Este es un párrafo muy pequeño que se encuentra dentro de la etiqueta <b>p</b> de <i>html</i></p>
           </body>
           </html>
```
* `(1)` Tipo de documento (indica que estamos trabajando con html5)
* `(2)` Etiqueta de inicio
* `(3)` Etiqueta de Cabeza
* `(4)` Codificación
* `(5)` Titulo de la pagina web
* `(6)` Cuerpo (contenido de la pagina)



<!---------------------------------------->
<!--- Introducción a datos geográficos --->
<!---------------------------------------->
# [2.] Aplicación en R
<html><div style='float:left'></div><hr color='#000099' size=3px width=850px></html>

<!---------------->
## Antes de hacer web-scraping...

Antes de hacer web-scraping vamos a leer los términos y condiciones de la pagina web. Se puede hacer agregando la ruta **/robots.txt** al final de la URL.

<div align="center">
<img src="pics/robots.gif">
</div>

<!---------------->
## Extraer el HTML

Vamos a usar las librerias `XML`, `rvest` y `xml2` 

```{r,eval=T,echo=T,warning=T}
require(pacman)
p_load(tidyverse,XML,rvest,xml2)

# read_html() lee el HTML de la pagina y lo convierte en un objeto del tipo 'xml_document' y 'xml_node'
myurl = "https://es.wikipedia.org/wiki/Organización_para_la_Cooperación_y_el_Desarrollo_Económicos"
myhtml = read_html(myurl)
class(myhtml)
```
<div align="center">
<img src="pics/myhtml.png" heigth=200>
</div>

<!---------------->
## Extraer elementos del HTML (usando xpath)

El lenguaje Xpath es el sistema que se utiliza para navegar y consultar los elementos y atributos contenidos en la estructura de un documento XML.

<div align="center">
<img src="pics/body.gif" heigth=500>
</div>

<!---------------->
## XML Path Language (xpath)

Vamos a inspeccionar los elementos para obtener la ruta dentro del HTML

<div align="center">
<img src="pics/xpath.png" heigth=500>
</div>

<!---------------->
## Extraer un texto
```{r,eval=T,echo=T,warning=T}
# extraer el primer parrafo de la pagina
myhtml %>% html_nodes(xpath = '//*[@id="mw-content-text"]/div/p[1]')

myhtml %>% html_nodes(xpath = '//*[@id="mw-content-text"]/div/p[1]') %>% 
class()

texto = myhtml %>% html_nodes(xpath = '//*[@id="mw-content-text"]/div/p[1]') %>% 
html_text() # Convertir en texto
texto
```


<!---------------->
## Extraer atributos de los elemtnos
```{r,eval=T,echo=T,warning=T}
# usando el atributo del elemento
myhtml %>% html_nodes(css = ".toctext") %>% html_text() # Extraemos los subtítulos de la pagina

myhtml %>% html_nodes(".toctext") %>% html_text() # Si no le indicamos que es un css, R reconoce que es un css

myhtml %>% html_nodes(xpath = ".toctext") %>% html_text() # Pero si usamos el xpath comete un error
```

<!---------------->
## Extraer usando el nodo
```{r,eval=T,echo=T,warning=T}
# html_node() vs html_nodes()
myhtml %>% html_nodes("a") # html_nodes() retorna el tipo de objeto y los 874 link que hay en la pagina
myhtml %>% html_nodes("a") %>% length()

myhtml %>% html_node("a") # html_node() retorna el tipo de objeto y el primer link de la pagina
myhtml %>% html_node("a") %>% length()
```

<!---------------->
## Extraer las URL a otras paginas
```{r,eval=T,echo=T,warning=T}
### 1.4. Extraer los link de las referencias
link = myhtml %>% html_nodes(xpath = '//*[@id="mw-content-text"]/div[1]/div[10]') # extraer el xml_nodeset de la sección de referencias
link

link = html_nodes(link,"a") # Extraer elementos que contienen un link (los que tienen la etiqueta a)

link = html_attr(link,'href') %>% as.data.frame() %>% setNames("link") # Extraer solo el link (atributo ref del elemento)

link = link %>% dplyr::filter(substr(.$link,1,4)=="http") # Filtrar solo los enlaces
link
```



<!--------------------->
<!--- Checklist --->
<!--------------------->
# Gracias
<html><div style='float:left'></div><hr color='#000099' size=3px width=850px></html>

<!--------------------->
## Hoy vimos

☑ Introducción a Web-scraping

☑ Aplicación en R

<!--------------------->
## Para seguir leyendo

* Munzert, Simon et al., 2015. Automated Data Collection with R: A Practical Guide to Web Scraping and Text Mining [[Ver aquí]](http://www.r-datacollection.com)

  + Chapter 2: HTML
  + Chapter 4: Path
  + Chapter 9: Scraping the Web

  
<!--- HTML style --->
<style type="text/css">
.reveal .progress {background: #CC0000 ; color: #CC0000}
.reveal .controls {color: #CC0000}
.reveal h1.title {font-size: 2.4em;color: #CC0000; font-weight: bolde}
.reveal h1.subtitle {font-size:2.0em ; color:#000000}
.reveal section h1 {font-size:2.0em ; color:#CC0000 ; font-weight:bolder ; vertical-align:middle}
.reveal section h2 {font-size:1.3em ; color:#CC0000 ; font-weight:bolder ; text-align:left}
.reveal section h3 {font-size:0.9em ; color:#00000 ; font-weight:bolder ; text-align:left}
.reveal section h4 {font-size:0.9em ; color:#CC0000 ; text-align:left}
.reveal section h5 {font-size:0.9em ; color:#00000 ; font-weight:bolder ; text-align:left}
.reveal section p {font-size:0.7em ; color:#00000 ; text-align:left}
.reveal section a {font-size:0.9em ; color:#000099 ; text-align:left}
.reveal section href {font-size:0.9em ; color:#000099 ; text-align:left}
.reveal section div {align="center";}
.reveal ul {list-style-type:disc ; font-size:0.8em ; color:#00000 ; display: block;}
.reveal ul ul {list-style-type: square; font-size:0.8em ; display: block;}
.reveal ul ul ul {list-style-type: circle; font-size:0.8em ; display: block;}
.reveal section img {display: inline-block; border: 0px; background-color: #FFFFFF; align="center"}
</style>

